﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMSDB.Repository;

namespace PMSDB.UnitOfWork
{
    public class UnitOfWork:IUnitOfWork
    {
        private PSMEntities _context;

        public UnitOfWork(PSMEntities context = null)
        {
            _context = context ?? new PSMEntities();
        }

        private IRepository<User> _userRepository;

        public IRepository<User> UserRepository => _userRepository ?? (_userRepository = new RepositoryBase<User>(_context));

        private IRepository<Menu> _menuRepository;

        public IRepository<Menu> MenuRepository => _menuRepository ?? (_menuRepository = new RepositoryBase<Menu>(_context));

        private IRepository<Role> _roleRepository;

        public IRepository<Role> RoleRepository => _roleRepository ?? (_roleRepository = new RepositoryBase<Role>(_context));


        private IRepository<RoleAccess> _roleAccessRepository;

        public IRepository<RoleAccess> RoleAccessRepository => _roleAccessRepository ?? (_roleAccessRepository=new RepositoryBase<RoleAccess>(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
