﻿using PMSDB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSDB.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }

        IRepository<Menu> MenuRepository { get; }

        IRepository<Role> RoleRepository { get; }

        IRepository<RoleAccess> RoleAccessRepository { get; }

        void Save();

        void Dispose();

    }
}
