﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PMSDB.Repository
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        internal PSMEntities _context;

        internal DbSet<T> _dbSet;

        public RepositoryBase(PSMEntities context)
        {
            _context = context;

            _dbSet = context.Set<T>();
        }

        public int Count => _dbSet.Count();

        public IEnumerable<T> All() => _dbSet.AsEnumerable<T>().ToList();

        public bool Contains(System.Linq.Expressions.Expression<Func<T, bool>> predicate) => _dbSet.Count(predicate) > 0;

        public T Create(T t)
        {
            try
            {
                var newEntry = _dbSet.Add(t);

                _context.SaveChanges();

                return newEntry;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var error in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                      error.Entry.Entity.GetType().Name, error.Entry.State);
                    foreach (var ve in error.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public virtual void Delete(object id)
        {
            T tToDelete = _dbSet.Find(id);

            Delete(tToDelete);
        }

        public virtual void Delete(T t)
        {
            if (_context.Entry(t).State == System.Data.Entity.EntityState.Detached)
            {
                _dbSet.Attach(t);
            }

            _dbSet.Remove(t);
        }

        public int Delete(Expression<Func<T, bool>> predicate)
        {
            var obj = Filter(predicate);

            foreach (var objects in obj)
            {
                _dbSet.Remove(objects);
            }

            return _context.SaveChanges();
        }

        public virtual void DetachAndUpdate(DbContext ctxt, T orginalT, T updatedT)
        {
            var objContext = ((IObjectContextAdapter)ctxt).ObjectContext;

            var objSet = objContext.CreateObjectSet<T>();

            var entityKey = objContext.CreateEntityKey(objSet.EntitySet.Name, orginalT);

            Object foundT;

            var exists = objContext.TryGetObjectByKey(entityKey, out foundT);

            if (!exists)
            {
                ctxt.Set<T>().Attach(updatedT);

                ctxt.Entry(updatedT).State = EntityState.Modified;
            }
            else
            {
                objContext.Detach(foundT);

                ctxt.Entry(updatedT).State = EntityState.Modified;
            }

        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public IEnumerable<T> ExecReadWithStoreProcedure(string query, params object[] parameters)=> 
            _context.Database.SqlQuery<T>(query, parameters);

        public List<T> ExecuteSPwithParameterForList(string query, params object[] parameters)=> 
            _context.Database.SqlQuery<T>(query, parameters).ToList();

        public int ExecWithStoreProcedure(string query, params object[] parameters)=> 
            _context.Database.ExecuteSqlCommand(query, parameters);

        public virtual bool Exists(T t)
        {
            var objContext = ((IObjectContextAdapter)_context).ObjectContext;

            var objSet = objContext.CreateObjectSet<T>();

            var tKey = objContext.CreateEntityKey(objSet.EntitySet.Name, t);

            Object foundT;

            var exists = objContext.TryGetObjectByKey(tKey, out foundT);

            if (exists)
            {
                objContext.Detach(foundT);
            }

            return (exists);
        }

        public IQueryable<T> Filter(System.Linq.Expressions.Expression<Func<T, bool>> predicate)=> 
            _dbSet.Where(predicate).AsQueryable();

        public IQueryable<T> Filter(System.Linq.Expressions.Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 50)
        {

            int skipCount = index * size;

            var resetSet = filter != null ? _dbSet.Where(filter).AsQueryable() : _dbSet.AsQueryable();

            resetSet = skipCount == 0 ? resetSet.Take(size) : resetSet.Skip(skipCount).Take(size);

            total = resetSet.Count();

            return resetSet.AsQueryable();
        }

        public T Find(params object[] keys)=> _dbSet.Find(keys);

        public T Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate)=>
            _dbSet.SingleOrDefault(predicate);

        public virtual IEnumerable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeproperty) => current.Include(includeproperty));

            if (orderBy != null)
            {
                return orderBy(query).AsEnumerable();
            }

            return query.AsEnumerable();
        }

        public virtual T GetById(object id) => _dbSet.Find(id);

        public virtual IEnumerable<T> GetOrderBy(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, System.Linq.Expressions.Expression<Func<T, bool>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeproperty) => current.Include(includeproperty));

            if (orderBy != null)
            {
                return query.OrderBy(orderBy);
            }

            return query.AsEnumerable();
        }

        public virtual IEnumerable<T> GetWithSql(string query, params object[] parameters)=> 
            _dbSet.SqlQuery(query, parameters).ToList();

        public void Insert(T t)=> _dbSet.Add(t);

        public virtual int Update(T t)
        {
            if (!Exists(t))
            {
                _dbSet.Attach(t);
            }

            _context.Entry(t).State = EntityState.Modified;

            return _context.SaveChanges();
        }

        public virtual void Update(T oldT, T newT)
        {
            if (!Exists(newT))
            {
                _dbSet.Attach(newT);
            }

            _context.Entry(oldT).State = EntityState.Modified;

            _context.SaveChanges();
        }

        public object SortDirection { get; private set; }

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
    }
}
